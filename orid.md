# O

今天我们学习了concept map的相关知识，同时进行团队协作画了一个关于什么是Internet的concept map。

# R

画concept map 的过程中思维太容易发散，导致不仅整个concept map的结构混乱不美观，而且可能有些偏离focus question。

# I

为什么会发散？因为对focus question进行简单分解后就对每一个单独的concept进行展开，什么有关写什么。导致忽略了focus question。

# D

画concept map的时候还是要注意focus question，关注你写的新concept是否能对focus question提供帮助，而不是只是单纯的解释概念。